<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

/**
 * DumpController
 * Load notifications and events data into db
 */
class DumpController extends Controller
{

    public function actionLoad()
    {
        $db = Yii::$app->db;

        $db->createCommand('
        INSERT INTO `events` (`id`, `title`, `event_type`, `model_class`, `additional_expression`, `created_at`, `updated_at`) VALUES
(1, \'Самостоятельная регистрация пользователя\', \'afterInsert\', \'app\\\models\\\Users\', \'return Yii::$app->user->identity->role !== \\\app\\\models\\\Users::ROLE_ADMIN;\', \'2017-02-02 22:19:57\', \'2017-02-06 10:54:03\'),
(2, \'Создание пользователя админом\', \'afterInsert\', \'app\\\models\\\Users\', \'return Yii::$app->user->identity->role == \\\app\\\models\\\Users::ROLE_ADMIN;\', \'2017-02-06 10:53:23\', \'2017-02-06 10:53:23\'),
(3, \'Создание новой новости\', \'afterInsert\', \'app\\\models\\News\', \'\', \'2017-02-06 11:00:17\', \'2017-02-07 15:41:52\'),
(4, \'Обновление пароля пользователя\', \'beforeUpdate\', \'app\\\models\\\Users\', \'return $model->oldAttributes[\'\'password_hash\'\'] != $model->password_hash;\', \'2017-02-06 11:28:33\', \'2017-02-06 11:33:43\');
        ')->execute();

        $db->createCommand('
        INSERT INTO `notifications` (`id`, `title`, `subject`, `text`, `methods`, `receiver_class`, `receiver_field`, `receiver_field_value`, `created_at`, `updated_at`) VALUES
            (2, \'Ссылка для активации email\', \'Активация email\', \'Для активации вашего email пройдите по ссылке <a href="{activationUrl}">{activationUrl}</a>\', \'["email"]\', \'app\\\models\\\Users\', \'\', NULL, \'2017-02-06 10:18:57\', \'2017-02-06 10:18:57\'),
            (3, \'Создание пользователя админом\', \'Вас зарегистрировали\', \'Для того, чтобы задать пароль и активировать ваш email пройдите по ссылке <a href="{activationUrl}">{activationUrl}</a>\', \'["email"]\', \'app\\\models\\\Users\', \'\', NULL, \'2017-02-06 10:54:58\', \'2017-02-06 10:54:58\'),
            (4, \'Добавлена новая новость\', \'Добавлена новая новость\', \'Добавлена новая новость. Заголовок: {title},  <a href="{url}">ссылка</a>\', \'["email","browser"]\', \'app\\\models\\\Users\', \'role\', \'["1","10","99"]\', \'2017-02-06 11:05:27\', \'2017-02-06 11:05:27\'),
            (5, \'Зарегистрирован новый пользователь\', \'Зарегистрирован новый пользователь\', \'Зарегистрирован новый пользователь. Email: {email}\', \'["email"]\', \'app\\\models\\\Users\', \'role\', \'["99"]\', \'2017-02-06 11:09:22\', \'2017-02-06 11:09:22\'),
            (6, \'Оповещение о изменении пароля\', \'Ваш пароль изменен\', \'Ваш пароль изменен\', \'["email"]\', \'app\\\models\\\Users\', \'\', \'""\', \'2017-02-06 11:29:24\', \'2017-02-06 11:29:24\');
        ')->execute();

        $db->createCommand('
        INSERT INTO `owner_notification` (`id`, `owner_id`, `notification_id`) VALUES
            (1, 1, 1),
            (2, 1, 2),
            (3, 2, 3),
            (5, 1, 5),
            (6, 4, 6),
            (7, 3, 4);
        ')->execute();
    }
}
