<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

/**
 * Class RbacController
 * @package app\commands
 */
class RbacController extends Controller
{
    /**
     * Generate RBAC files
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        //news CRUD

        $createNews = $auth->createPermission('createNews');
        $createNews->description = 'Create a news';
        $auth->add($createNews);

        $updateNews = $auth->createPermission('updateNews');
        $updateNews->description = 'Update news';
        $auth->add($updateNews);

        $deleteNews = $auth->createPermission('deleteNews');
        $deleteNews->description = 'Update news';
        $auth->add($deleteNews);

        $viewDetailNews = $auth->createPermission('viewDetailNews');
        $viewDetailNews->description = 'View detail news';
        $auth->add($viewDetailNews);

        $viewNewsList = $auth->createPermission('viewNewsList');
        $viewNewsList->description = 'View news list';
        $auth->add($viewNewsList);
        // ends news CRUD

        // users CRUD

        $createUsers = $auth->createPermission('createUsers');
        $createUsers->description = 'Create a Users';
        $auth->add($createUsers);

        $updateUsers = $auth->createPermission('updateUsers');
        $updateUsers->description = 'Update Users';
        $auth->add($updateUsers);

        $deleteUsers = $auth->createPermission('deleteUsers');
        $deleteUsers->description = 'Update Users';
        $auth->add($deleteUsers);

        $manageUsers = $auth->createPermission('manageUsers');
        $manageUsers->description = 'Manage Users';
        $auth->add($manageUsers);

        $viewUsers = $auth->createPermission('viewUsers');
        $viewUsers->description = 'View Users';
        $auth->add($viewUsers);

        // Events

        $manageEvents = $auth->createPermission('manageEvents');
        $manageEvents->description = 'Manage events module';
        $auth->add($manageEvents);

        // Notifications

        $manageNotifications = $auth->createPermission('manageNotifications');
        $manageNotifications->description = 'Manage notifications module';
        $auth->add($manageNotifications);

        // ends users CRUD

        // guest user

        $guest = $auth->createRole('guest');
        $auth->add($guest);
        $auth->addChild($guest, $viewNewsList);

        // role user

        $user = $auth->createRole('user');
        $auth->add($user);
        $auth->addChild($user, $viewDetailNews);
        $auth->addChild($user, $guest);

        // ends role user

        // role moderator

        $moderator = $auth->createRole('moderator');
        $auth->add($moderator);
        $auth->addChild($moderator, $createNews);
        $auth->addChild($moderator, $updateNews);
        $auth->addChild($moderator, $user);

        // ends role moderator

        // role admin

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($admin, $createUsers);
        $auth->addChild($admin, $updateUsers);
        $auth->addChild($admin, $deleteUsers);
        $auth->addChild($admin, $manageNotifications);
        $auth->addChild($admin, $manageEvents);
        $auth->addChild($admin, $deleteNews);
        $auth->addChild($admin, $moderator);

        $auth->assign($admin, 1);
        // ends role admin

    }
}
