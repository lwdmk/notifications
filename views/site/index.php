<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SearchNews */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if(Yii::$app->user->can('createNews')) { ?>
            <?= Html::a('Create News', ['news/create'], ['class' => 'btn btn-success']) ?>
        <?php }?>
    </p>
    <? \yii\widgets\Pjax::begin(['id' => 'main_page_news'])?>
        <?= Html::dropDownList('perPage', $searchModel->perPage, $searchModel->getPerPageValues(), [
            'onchange' => '$.pjax.reload("#main_page_news", {
                url : \'/\' + \'?per-page=\' + $(this).val()
            });',
        ]);?>
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_news_item',
        ]); ?>
    <? \yii\widgets\Pjax::end();?>
</div>
