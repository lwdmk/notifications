<?php
/* @var $model \app\models\News */
?>
<div class="news_item">
    <div class="news_title"><?=$model->title;?></div>
    <div class="news_short_description"><?=$model->short_description;?></div>
    <?php if (Yii::$app->user->can('viewDetailNews')) { ?>
        <div class="news_link"><?=\yii\helpers\Html::a('details...', $model->url);?></div>
    <?php } ?>
</div>