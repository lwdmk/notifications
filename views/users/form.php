<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = (($model->isNewRecord) ? 'Create' : 'Update').' user';;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-create">

    <h1><?=Html::encode($this->title)?></h1>

    <div class="users-form">

        <?php $form = \yii\widgets\ActiveForm::begin(); ?>

        <?=$form->field($model, 'email')->textInput(['maxlength' => true])?>

        <?=$form->field($model, 'role')->dropDownList(\app\models\Users::getRoles())?>

        <?=$form->field($model, 'group_id')->dropDownList(\app\models\Users::getGroups())?>

        <?=$form->field($model, 'is_email_activated')->checkbox()?>

        <div class="form-group">
            <?=Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
        </div>

        <?php \yii\widgets\ActiveForm::end(); ?>

    </div>
</div>
