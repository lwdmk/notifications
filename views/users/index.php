<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SearchUsers */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?=Html::encode($this->title)?></h1>

    <p>
        <?=Html::a('Create Users', ['create'], ['class' => 'btn btn-success'])?>
    </p>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'email:email',
            [
                'attribute' => 'role',
                'filter'    => \app\models\Users::getRoles(),
                'value'     => function ($model) {
                    $array = \app\models\Users::getRoles();

                    return array_key_exists($model->role, $array) ? $array[$model->role] : '';
                }
            ],
            [
                'attribute' => 'group_id',
                'filter'    => \app\models\Users::getGroups(),
                'value'     => function ($model) {
                    $array = \app\models\Users::getGroups();

                    return array_key_exists($model->group_id, $array) ? $array[$model->group_id] : '';
                }
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]);?>
</div>
