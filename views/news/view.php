<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?=Html::encode($this->title)?></h1>

    <p>
        <?php if (Yii::$app->user->can('updateNews')) { ?>
            <?=Html::a('Update', ['news/update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
        <?php } ?>
        <?php if (Yii::$app->user->can('deleteNews')) { ?>
            <?=Html::a('Delete', ['news/delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method'  => 'post',
                ],
            ])?>
        <?php } ?>
    </p>

    <?=DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'title',
            'short_description:ntext',
            'full_text:ntext',
            'author_id',
            'created_at',
            'updated_at',
        ],
    ])?>

</div>
