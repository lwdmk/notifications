<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */

$this->title = (($model->isNewRecord) ? 'Create' : 'Update').' news';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="news-form">
    <h1><?=Html::encode($this->title)?></h1>

    <div class="users-form">

        <?php $form = ActiveForm::begin(); ?>

        <?=$form->field($model, 'title')->textInput(['maxlength' => true])?>

        <?=$form->field($model, 'short_description')->textarea(['rows' => 6])?>

        <?=$form->field($model, 'full_text')->textarea(['rows' => 6])?>

        <div class="form-group">
            <?=Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
