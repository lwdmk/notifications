<?php

namespace app\controllers;

use app\models\forms\SetPasswordForm;
use app\models\forms\SignupForm;
use app\models\search\SearchNews;
use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use yii\web\NotFoundHttpException;

/**
 * SiteController servicing main site operations
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['guest'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Lists all News preview.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchNews();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Login page
     *
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Registration Page
     *
     * @return string|\yii\web\Response
     */
    public function actionSignUp()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            return $this->goBack();
        }
        return $this->render('sign-up', [
            'model' => $model,
        ]);
    }

    /**
     * Automatic email activation for registered user and password setting page for user registered by admin
     *
     * @param $token
     *
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionActivation($token)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $user = Users::findIdentityByAccessToken($token);
        /* @var $user Users */
        if($user) {
            $user->is_email_activated = Users::EMAIL_ACTIVATED;
            if(empty($user->password_hash)) {
                $model = new SetPasswordForm();
                if (!$model->load(Yii::$app->request->post()) || (null == $model->setPassword($user))) {
                    return $this->render('set-password', [
                        'model' => $model,
                    ]);
                }
            }
            $user->email_activation_token = '';
            if($user->save(false, ['is_email_activated', 'email_activation_token'])) {
                Yii::$app->user->login($user);
                return $this->goHome();
            } else {
                throw new BadRequestHttpException();
            }
        }
        throw new NotFoundHttpException();
    }

    /**
     * Logout
     *
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
