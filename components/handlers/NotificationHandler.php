<?php

namespace app\components\handlers;

use app\modules\events\interfaces\EventHandlerInterface;
use app\modules\notifications\models\Notifications;
use app\modules\notifications\models\OwnerNotification;
use yii\base\Component;

/**
 * Class NotificationHandler
 * @package app\components\handlers
 */
class NotificationHandler extends Component implements EventHandlerInterface
{
    /**
     * @inheritdoc
     */
    public function handle($event_model, $yii_event)
    {
        $notifications  = Notifications::find()->alias('n')
            ->leftJoin(['on' => OwnerNotification::tableName()], 'on.notification_id = n.id')
            ->where(['on.owner_id' => $event_model->id])
            ->all();
        /* @item Notifications */
        foreach($notifications as $item){
            $item->send($yii_event->sender);
        }
    }
}
