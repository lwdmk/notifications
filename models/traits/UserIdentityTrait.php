<?php
namespace app\models\traits;

use Yii;
use app\models\Users;

trait UserIdentityTrait
{
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return Users::findOne(['id' => $id, 'is_email_activated' => Users::EMAIL_ACTIVATED]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return Users::findOne(['email_activation_token' => $token, 'is_email_activated' => 0]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityHash($token)
    {
        return Users::findOne(['hash' => $token, 'activated' => 0]);
    }

    /**
     * @inheritdoc
     */
    public static function findByPasswordResetToken($token)
    {
        if (! static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return Users::findOne([
            'password_reset_token' => $token,
            'is_email_activated'   => Users::EMAIL_ACTIVATED,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateChangePassword($attribute, $params)
    {
        if (! $this->validatePassword($this->old_password)) {
            $this->addError($attribute, 'Неверно указан старый пароль');
        }
    }

    /**
     * @inheritdoc
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * @inheritdoc
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @inheritdoc
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString().'_'.time();
    }

    /**
     * @inheritdoc
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
