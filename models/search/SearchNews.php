<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\News;

/**
 * SearchUsers represents the model behind the search form about `app\models\News`.
 */
class SearchNews extends News
{
    /**
     * @var integer $perPage
     */
    public $perPage;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author_id'], 'integer'],
            [['title', 'short_description', 'alias', 'full_text', 'perPage',], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Search for News ListView
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find()->alias('news');
        $query->orderBy('news.created_at, news.id');

        $this->perPage = ($params['per-page'] && in_array($params['per-page'], $this->getPerPageValues()))
            ? $params['per-page']
            : Yii::$app->params['perPageDefault'];
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->perPage,
            ],
        ]);

        return $dataProvider;
    }

    /**
     * Getting list of per page vaules
     *
     * @return array
     */
    public function getPerPageValues()
    {
        return [
            Yii::$app->params['perPageDefault'] => Yii::$app->params['perPageDefault'],
            10 => 10,
            15 => 15
        ];
    }
}
