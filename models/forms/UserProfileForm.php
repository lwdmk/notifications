<?php
namespace app\models\forms;

use Yii;
use app\models\Users;
use yii\base\Model;

/**
 * User Profile form
 */
class UserProfileForm extends Model
{
    /**
     * @var string $password
     */
    public $password;

    /**
     * @var string $password_confirm
     */
    public $password_confirm;

    /**
     * @var array $notification_settings_array
     */
    public $notification_settings_array = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'string', 'min' => 6],
            ['password_confirm', 'compare', 'compareAttribute' => 'password'],
            [['notification_settings_array'], 'safe']

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password'                     => 'Password',
            'password_confirm'             => 'Confirm password',
            'notification_settings_array'  => 'Notification settings',
        ];
    }

    /**
     * Saving profile data to user model
     *
     * @param Users $user
     * @return null
     */
    public function saveTo($user)
    {
        if ($this->validate()) {
            $user->notification_settings = json_encode($this->notification_settings_array);
            if(!empty($this->password)) {
                $user->setPassword($this->password);
            }

            return $user->save(true, ['password_hash', 'notification_settings']) ? $user : null;
        }

        return null;
    }
}
