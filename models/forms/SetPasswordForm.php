<?php
namespace app\models\forms;

use Yii;
use app\models\Users;
use yii\base\Model;

/**
 * Set Password for users have been created by admin
 */
class SetPasswordForm extends Model
{
    /**
     * @var string $password
     */
    public $password;

    /**
     * @var string $password_confirm
     */
    public $password_confirm;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['password_confirm', 'required'],
            ['password_confirm', 'compare', 'compareAttribute' => 'password'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'password'         => 'Password',
            'password_confirm' => 'Confirm password',
        ];
    }

    /**
     * Set user password.
     *
     * @param Users $user
     * @return Users|null the saved model or null if saving fails
     */
    public function setPassword($user)
    {
        if ($this->validate()) {
            $user->setPassword($this->password);

            return $user->save(true, ['password_hash']) ? $user : null;
        }

        return null;
    }
}
