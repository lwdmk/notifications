<?php

namespace app\models;

use Yii;
use app\models\traits\UserIdentityTrait;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $email
 * @property integer $role
 * @property integer $group_id
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email_activation_token
 * @property integer $is_email_activated
 * @property string $notification_settings
 * @property string $created_at
 * @property string $updated_at
 *
 * @property News[] $news
 */
class Users extends ActiveRecord implements IdentityInterface
{

    use UserIdentityTrait;

    /**
     * Values of is_email_activated field
     */
    const EMAIL_ACTIVATED = 1, EMAIL_NOT_ACTIVATED = 0;

    /**
     * User roles
     */
    const ROLE_ADMIN = 99, ROLE_MODERATOR = 10, ROLE_USER = 1;

    /**
     * User groups
     */
    const GROUP_1 = 1, GROUP_2 = 2, GROUP_3 = 3;

    /**
     * @var array $notification_settings_array Array representation of $notification_settings JSON attribute
     */
    public $notification_settings_array = [];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value'              => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */

    public function init()
    {
        if($this->isNewRecord) {
            $this->notification_settings_array = (\Yii::$app->getModule('notifications'))
                ? array_keys(\Yii::$app->getModule('notifications')->getTransportList())
                : [];
        }
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
            [['role', 'is_email_activated', 'group_id'], 'integer'],
            [['is_email_activated'], 'default', 'value' => self::EMAIL_NOT_ACTIVATED],
            [['role'], 'default', 'value' => self::ROLE_USER],
            [['email_activation_token'], 'default', 'value' => Yii::$app->security->generateRandomString()],
            [['created_at', 'updated_at'], 'safe'],
            [
                ['email', 'password_hash', 'password_reset_token', 'email_activation_token', 'notification_settings'],
                'string',
                'max' => 200
            ],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                     => 'ID',
            'email'                  => 'Email',
            'role'                   => 'Role',
            'password_hash'          => 'Password Hash',
            'password_reset_token'   => 'Password Reset Token',
            'email_activation_token' => 'Email Activation Token',
            'is_email_activated'     => 'Is Email Activated',
            'created_at'             => 'Created At',
            'updated_at'             => 'Updated At',
        ];
    }

    /**
     * Url to email activation page
     *
     * @return string
     */
    public function getActivationUrl()
    {
        return Url::toRoute(['site/activation', 'token' => $this->email_activation_token], true);
    }

    /**
     * @return News
     */
    public function getNews()
    {
        return $this->hasMany(News::className(), ['author_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getRoles()
    {
        return [
            self::ROLE_USER      => 'user',
            self::ROLE_MODERATOR => 'moderator',
            self::ROLE_ADMIN     => 'admin',
        ];
    }

    /**
     * @return array
     */
    public static function getGroups()
    {
        return [
            self::GROUP_1 => 'GROUP_1',
            self::GROUP_2 => 'GROUP_2',
            self::GROUP_3 => 'GROUP_3',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $this->notification_settings = json_encode($this->notification_settings_array);
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->processDefaultRoleAssign($insert, $changedAttributes);
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        $auth = \Yii::$app->authManager;
        $auth->revoke($auth->getRole(array_search($this->oldAttributes['role'], array_flip(self::getRoles()))),
            $this->id);

        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $notification_settings_decoded = json_decode($this->notification_settings, true);
        $this->notification_settings_array = (false == $notification_settings_decoded) ? [] : $notification_settings_decoded;
        parent::afterFind();
    }

    /**
     * Assign role to new/updated user due to $role attribute
     *
     * @param $insert
     * @param $changedAttributes
     * @return void
     */
    protected function processDefaultRoleAssign($insert, $changedAttributes)
    {
        if ($insert || $changedAttributes['role']) {
            $auth = \Yii::$app->authManager;
            if (! $insert) {
                $old_auth_role = $auth->getRole(array_search($this->oldAttributes['role'],
                    array_flip(self::getRoles())));
                $assignment = $auth->getAssignment($old_auth_role, $this->id);
                if ($assignment) {
                    $auth->revoke($old_auth_role, $this->id);
                }
            }
            $role = $auth->getRole(array_search($this->role, array_flip(self::getRoles())));
            $auth->assign($role, $this->id);
        }
    }
}
