<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $short_description
 * @property string $full_text
 * @property integer $author_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property string $url
 * @property Users $author
 */
class News extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value'              => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'short_description', 'full_text'], 'required'],
            [['short_description', 'full_text'], 'string'],
            [['author_id'], 'integer'],
            [['author_id'], 'default', 'value' => Yii::$app->user ? Yii::$app->user->id : 0],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 120],
            [
                ['author_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Users::className(),
                'targetAttribute' => ['author_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'title'             => 'Title',
            'short_description' => 'Short Description',
            'full_text'         => 'Full Text',
            'author_id'         => 'Author ID',
            'created_at'        => 'Created At',
            'updated_at'        => 'Updated At',
        ];
    }

    /**
     * Url to news item page
     *
     * @return string
     */
    public function getUrl()
    {
        return Url::toRoute(['news/view', 'id' => $this->id], true);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Users::className(), ['id' => 'author_id']);
    }
}
