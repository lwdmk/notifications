<?php

$params = require(__DIR__.'/params.php');

$config = [
    'id'         => 'basic',
    'basePath'   => dirname(__DIR__),
    'bootstrap'  => ['log', 'events'],
    'modules'    => [
        'events'        => [
            'class'      => 'app\modules\events\Module',
            'modelPaths' => ['@app/models'],
            'handler'    => [
                'class' => 'app\components\handlers\NotificationHandler'
            ]
        ],
        'notifications' => [
            'class'                     => 'app\modules\notifications\Module',
            'ownerClass'                => 'app\modules\events\models\Events',
            'ownerPkField'              => 'id',
            'ownerTitleField'           => 'title',
            'receiverClass'             => 'app\models\Users',
            'notificationSettingsField' => 'notification_settings_array',
            'receiverFields'            => [
                [
                    'fieldName'         => 'role',
                    'fieldValuesMethod' => 'getRoles'
                ],
                [
                    'fieldName'         => 'group_id',
                    'fieldValuesMethod' => 'getGroups'
                ],
            ],
            'transports'                => [
                'email'   => [
                    'class'      => 'app\modules\notifications\models\transports\EmailTransport',
                    'emailField' => 'email',
                ],
                'browser' => [
                    'class' => 'app\modules\notifications\models\transports\BrowserTransport',
                ],
            ]
        ]
    ],
    'components' => [
        'request'      => [
            'cookieValidationKey' => 'FE1KJMpsbo287Ysd5I7OxIGFipfopr1y',
        ],
        'cache'        => [
            'class' => 'yii\caching\FileCache',
        ],
        'user'         => [
            'identityClass'   => 'app\models\Users',
            'enableAutoLogin' => true,
        ],
        'authManager'  => [
            'class'        => 'yii\rbac\PhpManager',
            'defaultRoles' => ['guest'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer'       => [
            'class'            => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'trace'],
                ],
            ],
        ],
        'db'           => require(__DIR__.'/db.php'),

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                '/' => 'site/index'
            ],
        ],
    ],
    'params'     => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
}

return $config;
