<?php

use yii\db\Migration;

/**
 * Class m170201_115814_news_table
 * Creating table for news
 */
class m170201_115814_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%news}}',[
            'id'                     => $this->primaryKey(),
            'title'                  => $this->string(120)->notNull(),
            'short_description'      => $this->text()->notNull(),
            'full_text'              => $this->text()->notNull(),
            'author_id'              => $this->integer(),
            'created_at'             => $this->dateTime(),
            'updated_at'             => $this->dateTime()
        ]);
        $this->createIndex('news_author_id_index', 'news', 'author_id');
        $this->addForeignKey('news_author_id', '{{%news}}', 'author_id','{{%users}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%news}}');
    }
}
