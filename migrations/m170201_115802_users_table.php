<?php

use yii\db\Migration;

/**
 * Class m170201_115802_users_table
 * Creating table for users
 */
class m170201_115802_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%users}}',[
            'id'                     => $this->primaryKey(),
            'email'                  => $this->string(200)->notNull()->unique(),
            'role'                   => $this->integer(1)->notNull()->defaultValue(1),
            'group_id'               => $this->smallInteger(),
            'password_hash'          => $this->string(200),
            'password_reset_token'   => $this->string(200),
            'email_activation_token' => $this->string(200),
            'is_email_activated'     => $this->integer(1)->defaultValue(0),
            'notification_settings'  => $this->text(),
            'created_at'             => $this->dateTime(),
            'updated_at'             => $this->dateTime()
        ]);

        $this->execute("
        INSERT INTO `users`
        (`id`, `email`, `role`,`group_id`, `password_hash`, `password_reset_token`, `email_activation_token`, `is_email_activated`,`notification_settings`, `created_at`, `updated_at`)
         VALUES
        (1, 'admin@admin.ru', 99, NULL, '$2y$13\$bw6ZJKtmnmwmkr.rcCRKhOcfK41kVxVbTM2BKKYpI4swMiw02ku0q', NULL, NULL, 1, '[\"email\",\"browser\"]',
         '2017-02-01 00:59:51', '2017-02-01 00:59:51');");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%users}}');
    }
}
